# Docker Workshop
Lab 08: Managing Containers

---

## Overview

In this lab you will understand all the commands used to manage your docker workloads and your docker hosts

## Preparations

 - Create a new folder for the lab:
```
$ mkdir ~/lab-08
$ cd ~/lab-08
```

## Managing Containers

 - Run the following containers using dockerhub images:
```
$ docker run -d -p 80:80 --name app-1 selaworkshops/counter:latest
```
```
$ docker run -d -p 8080:80 --name app-2 selaworkshops/counter:latest
```

 - Ensure the containers are running:
```
$ docker ps
```

 - Access the first container and make some clicks to increment the counter:
```
http://<your-server-ip>
```

 - Stop the first container:
```
$ docker stop app-1
```

 - Kill the second container:
```
$ docker kill app-2
```

 - Display running containers:
```
$ docker ps
```

 - Show all the containers (includind non running containers):
```
$ docker ps -a
```

 - Let's start both containers again:
```
$ docker start app-1 app-2
```

 - Browse the first container, note that the counter start from zero (the container process started again):
```
http://<your-server-ip>
```

 - Restart the second container:
```
$ docker restart app-2
```

 - Browse the first container and make some clicks to increment the counter:
```
http://<your-server-ip>
```

 - Let's pause the first container:
```
$ docker pause app-1
```

 - Show all the containers (note that the first container is in status paused):
```
$ docker ps -a
```

 - Try to browse to the first container (you can't reach it due it process is paused):
```
http://<your-server-ip>
```

 - Let's unpause the first container:
```
$ docker unpause app-1
```

 - Browse the first container, note that the counter remained in the same number (the container process started from the same point from which it was paused):
```
http://<your-server-ip>
```

 - Finally, let's see how to copy files from the container into the host file system (and vice versa). But first, check your lab directory content:
```
ls ~/lab-08
```

 - Now copy the container binary (counter-linux) into your host filesystem
```
docker cp app-1:/counter-linux ~/lab-08
```
```
ls ~/lab-08
```

 - Create a new file in your host filesystem
```
touch ~/lab-08/test
```

 - Copy the file into the container
```
docker cp ~/lab-08/test app-1:/
```


## Debugging Containers

 - Show the logs of the first container using the flag --follow:
```
$ docker logs --follow app-1
```

 - Browse to the application and see the containers logs from the terminal:
```
http://<your-server-ip>:80
```

 - Stop to tracking logs by run:
 ```
$ CTRL + C
```

 - Show the running processes in the first container using:
```
$ docker top app-1
```

 - Display the resource usage statistics of both containers (exit with Ctrl + C):
```
$ docker stats app-1 app-2
```

 - Retrieve the history of the container image:
```
$ docker history selaworkshops/counter:latest
```

 - Inspect the container image:
```
$ docker inspect selaworkshops/counter:latest
```

 - Inspect the first container:
```
$ docker inspect app-1
```

 - To check changes to files or directories on a container’s filesystem run
```
docker diff app-1
```


## Managing Hosts

 - Display the docker host information with:
```
$ docker info
```

 - Get real time events from the docker daemon using:
```
$ docker events
```

 - In another terminal, stop the second container (see the logs in the first terminal)
```
$ docker rm -f app-2
```


## Cleanup

 - Remove containers:
```
$ docker rm -f $(docker ps -a -q)
```

 - Remove images:
```
$ docker rmi $(docker images -q)
```


